<?php
namespace Snfapi\ApplicationAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\RedirectResponse,
	Symfony\Component\Security\Core\Util\SecureRandom,
	Symfony\Component\DependencyInjection\Exception\RuntimeException,
	
    /* Symfony annotation */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache,
        
    /* SnfApi Core */    
    Snfapi\CoreBundle\Service\Helper,
    Snfapi\CoreBundle\Service\MyrtisFacebook,
    Snfapi\CoreBundle\Service\Config,
	
	/* Entity */
	Snfapi\CoreBundle\Entity\EnvTracking,
	Snfapi\CoreBundle\Entity\AppTracking
;

class AdminController extends Controller {
    
    /**
     * @Route("/admin/{page}", name="admin", requirements={"page" = "\d+"}, defaults={"page" = 1})
	 * @Template
     */
	function adminAction($page, Request $request) {
		$rows = 5;
		
		$id			= '%%';
		$fb_uid 	= '%%';
		$user_agent = '%%';
		$ip 		= '%%';
		$date 		= '%%';
		
		$env_tracking = new EnvTracking;
		
		$form_builder = $this->createFormBuilder($env_tracking)
            ->add('id', 'text', array(/*'required' => false, */'attr' => array('placeholder' => 'ID')))
            ->add('fbUid', 'text', array(/*'required' => false, */'attr' => array('placeholder' => 'Facebook ID')))
            ->add('userAgent', 'text', array(/*'required' => false, */'attr' => array('placeholder' => 'User Agent')))
            ->add('ip', 'text', array(/*'required' => false, */'attr' => array('placeholder' => 'IP cím')))
            ->add('date', 'text', array(/*'required' => false*/))
            ->add('search', 'submit')
            ->getForm();
		
		$form_builder->handleRequest($request);
		
		if($form_builder->isValid()) {
			$data = $form_builder->getData();
			
			$id 		= '%'.$data->getId().'%';
			$fb_uid 	= '%'.$data->getFbUid().'%';
			$user_agent = '%'.$data->getUserAgent().'%';
			$ip 		= '%'.$data->getIp().'%';
			$date 		= '%'.$data->getDate().'%';
		}
	
		$em = $this->getDoctrine()->getManager();
		$count = $em->createQuery('
			SELECT COUNT(env_tracking.id)
			FROM SnfapiCoreBundle:EnvTracking env_tracking
		');
		$max = ceil($count->getSingleScalarResult() / $rows);
		
		$stmt = $em->createQuery("
			SELECT et.id, et.fbUid, et.userAgent, et.ip, et.date
			FROM SnfapiCoreBundle:EnvTracking et
			WHERE
				et.id LIKE :id AND
				et.fbUid LIKE :fbUid AND
				et.userAgent LIKE :userAgent AND
				et.ip LIKE :ip AND
				et.date LIKE :date
		");
		$stmt->setParameter('id', $id);
		$stmt->setParameter('fbUid', $fb_uid);
		$stmt->setParameter('userAgent', $user_agent);
		$stmt->setParameter('ip', $ip);
		$stmt->setParameter('date', $date);
		$stmt->setFirstResult($page * $rows - $rows);
		$stmt->setMaxResults($rows);
		$result = $stmt->getResult();
		$form = $form_builder->createView();
		
		return compact('page', 'max', 'result', 'form');
	}
	
	/**
	 * Bal menü - Entity list
	 *
	 * @Route("/admin_asside/", name="admin_asside")
	 * @Template
	 */
	function assideAction() {
		$entities = $this->getDoctrine()->getManager()->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
		
		return compact('entities');
	}
	
	/**
	 * Kiválasztott rekord részleteinek megtekintése
	 *
	 * @Route("/admin_details/{id}", name="admin_details")
	 * @Template
	 */
	function detailsAction($id, Request $request) {
		return array();
	}
}