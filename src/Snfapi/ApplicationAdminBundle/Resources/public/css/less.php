<?php
/**
 * Less compiler
 *
 * @author Simon Péter <peti@simonnetwork.hu>
 */
header ('Content-Type: text/css; Charset: UTF-8');
$env	= isset($_GET['env']) ? $_GET['env'] : 'dev';
$input	= 'index.less';

if(!is_dir('../../../../../../app/cache/prod/less')) {
	mkdir('../../../../../../app/cache/prod/less');
}

$cache = '../../../../../../app/cache/prod/less/'.$input.'.cache';

require_once '../../../../../../vendor/leafo/lessphp/lessc.inc.php';
ob_start('ob_gzhandler');

//gyorsítótár ellenőrzése
if($env == 'prod') {
	$data = file_exists($cache) ? unserialize(file_get_contents($cache)) : $input;
} else {
	$data = $input;
}

$less = new lessc;
$less->setFormatter('compressed');
$new = $less->cachedCompile($data);

if (!is_array ($data) || $new['updated'] > $data['updated']) {
	if($env == 'prod') {
		file_put_contents($cache, serialize ($new));
	}
    print ($new['compiled']);
} else
    print ($data['compiled']);

ob_end_flush();