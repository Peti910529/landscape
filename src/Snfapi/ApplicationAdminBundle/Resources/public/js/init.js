//=========================================================
// init.js
//---------------------------------------------------------

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}


$(function () {
    var container = $('#container');

    loadApiInit(217478378369588, '', 'dev');

    $(document)
        .ajaxStart (function () { $('#loader').slideDown(500); })
        .ajaxStop  (function () { $('#loader').slideUp(500); });
    
    container
        .ajaxload ({
            url_query   : container.data ('page'),
            dataType    : 'html',
            method      : 'post',
            cache       : false,
            pageStatus  : {
                200: function() { loadComponent(); },
                503: function() {
                    alert('Az alkalmazás karbantartás miatt szünetel!');
                }
            },
            noNetwork   : function() {
                alert('Nincs stabil internetkapcsolat!');
            }
        });
});

function loadComponent() {
	$('.box, .box img').mouseenter(function() {
		$(this).prev('.light-layer').show();
		$(this).children('.light-layer').show();
	});
	
	$('.box').mouseleave(function() {
		$(this).find('.light-layer').hide();
	});
	
	$('#close').on('click', function() {
		$('#msg').slideUp(500).empty();
	});
}
