<?php

namespace Snfapi\UtilityBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface,
    Twig_Extension;

class UtilityExtension extends Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct (ContainerInterface $container) {
        $this->container = $container;

        if ($this->container->isScopeActive ('request'))
            $this->request = $this->container->get ('request');
    }

	/**
	 * @return string
	 */
    public function getName() {
        return 'snfapi_util';
    }

	/**
	 * @return array
	 */
    public function getGlobals () {
        $globals = array ();

        if ($this->request && $this->request->get ('_template')) {
            $globals['ajax'] = isset ($_SERVER['HTTP_X_REQUESTED_WITH']); // jQuery ajax

			$template	= $this->request->get ('_template');
			if(is_object($template)) {
				$bundle = $template->get ('bundle');
			//ha egyéni @Template-et adunk meg, csak ezzel a  meoldással kérhetőek le a template paraméterei
			} else {
				$explode_template = explode(':', $template);
				$bundle = $explode_template[0];
			}
			
            $globals['bundle'] = array(
				'env'		=> $this->container->getParameter('kernel.environment'),
                'name'		=> $bundle,
                'assets'	=> 'bundles/'.strtolower (substr ($bundle, 0, -6)).'/',
            );
        }

        return $globals;
    }
}
