/**
 * @author Simon Péter <peti@simonnetwork.hu>
 */
(function ($) {
    var glob_options = {
		div 				: $('html'),
        cache       		: false,
		animation			: false,
        dataType    		: 'html',
        method      		: 'get',
        refresh     		: 0,
		loader_animation	: false,
        pageStatus  		: function() {  },
        noNetwork   		: function() {  }
    }
    
    function update_content (elm, data) {
        elm.html(data).show();
		glob_options.pageStatus[200].call(this);
		afterLoad (elm);
    }

    function prepareLoad (elm, url, data, clicked) {
        var data_cache      = $(clicked).attr('data-cache');
        var data_datatype   = $(clicked).attr('data-type');
        var data_method     = $(clicked).attr('data-method');
        var form_method     = $(clicked).attr('method');
        var data_refresh    = $(clicked).attr('data-refresh');
        var data_animation  = $(clicked).attr('data-animation');
        
        //gyorsítótárazás
        glob_options.cache =
            typeof clicked == 'undefined' || typeof data_cache == 'undefined' ?
                glob_options.cache :
                data_cache;
            
        //adattípus
        glob_options.dataType =
            typeof clicked == 'undefined' || typeof data_datatype == 'undefined' ?
                glob_options.dataType :
                data_datatype;
            
        //tartalom újratöltés    
        glob_options.refresh =
            typeof clicked == 'undefined' || typeof data_refresh == 'undefined' ?
                glob_options.refresh :
                data_refresh;
		
		//animáció típusa    
        glob_options.animation =
            typeof clicked == 'undefined' || typeof data_animation == 'undefined' ?
                glob_options.animation :
                data_animation;
        
        //ha nem formról van szó
        if(typeof form_method == 'undefined') {
            glob_options.method =
                typeof clicked == 'undefined' || typeof data_method == 'undefined' ?
                    glob_options.method :
                    data_method;
        } else
            glob_options.method = form_method;
        
        load(elm, url, '', data);
        
        //ha a tartalmat folyamatosan frissítheti
        if(glob_options.refresh != 0) {
            setInterval(function() {
                load(elm, url, options, data);
            }, glob_options.refresh);
        }
    }
    
	/**
	 * Ajax load
	 */
    function load(elm, url, options, data) {
        $.ajax
        ({
            type        : 'post',
            cache       : options.cache,
            url         : url,
            data        : data,
            dataType    : options.dataType,
            statusCode  : {
                //ha sikeresen betöltődött az oldal
                200: function(data) {
					window.history.pushState("", "", url.substr(0, url.length - 1));
					
                    elm.show(0, function () {
                        update_content (elm, data);
                    });
                },
                403: function() {
                    options.pageStatus[403].call(this);
                },
                404: function() {
                    options.pageStatus[404].call(this);
                },
                500: function() {
                    options.pageStatus[500].call(this);
                },
                503: function() {
                    options.pageStatus[503].call(this);
                }
            },
            //ha nincs internetkapcsolat
            error: function(statusCode) {
                if (statusCode.status == 0)
                    options.noNetwork.call(this);
            }
        });
    }
	
	function afterLoad (elm) {
        $('html').find ('a.ajaxload, a:not(.outer)')
			.unbind('click')
            .bind('click', function (ev) {
				var url_query   = $(this).attr ('href').split('?');
                var url         = url_query[0];
                var data        = url_query[1];
				var method		= typeof $(this).attr('data-method') == 'undefined' ? glob_options.div : $($(this).attr('data-method'));
				var rel			= typeof $(this).attr('rel') == 'undefined' ? glob_options.div : $($(this).attr('rel'));
				var animation	= typeof $(this).attr('data-animation') == 'undefined' ? glob_options.div : $($(this).attr('data-animation'));
				
				prepareLoad(rel, url, data, this);
                
                ev.preventDefault ();
            });

        $('html').find ('form')
			.unbind('click')
            .bind ('submit', function (ev) {
                prepareLoad (elm, $(this).attr ('action'), $(this).serialize(), this);
                ev.preventDefault ();
            });

        $('html').find ('form:has(input[type=file])')
			.unbind('click')
            .unbind('submit')
            .iframePostForm({
				'post'     : function () {
					$('#loader').slideDown(500);
				},
				'complete' : function (data) {
					elm.hide();
					update_content (elm, data);
					$('#loader').slideUp(500);
				}
			});

        elm.trigger ('widget-load');
    }

    $.fn.ajaxload = function (options, clicked) {	
        var url_query   = options.url_query.split('?');
        var url         = url_query[0];
		
		glob_options.div = this;
        $.extend(glob_options, options);
		
        if (!url || typeof (url) != 'string') return this;
        prepareLoad (this, url, url_query[1], clicked);

        return this;
    };
	
	$(document)
        .ajaxStart (function () { $('#loader').slideDown(500); })
        .ajaxStop  (function () { $('#loader').slideUp(500); });

})(jQuery);
