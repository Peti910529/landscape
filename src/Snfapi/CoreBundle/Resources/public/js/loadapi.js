/* Myrtis Creative - Javascript Makeup R0011
*
* R0001 - 2012.06.06. - Induló verziószám
* R0002 - 2012.06.07. - képernyő lekérés, flash lekérése, staisztikázás
* R0003 - 2012.06.11. - statisztikázásnál a felhasználó adatainak a naplózása rész eltávolítása
* R0004 - 2012.06.12.
*	- loadApi-ba új paraméter service néven. ha a service true akkor lefut a karbantartás alatti callback
*	- a felhasználó által elfogadott/nem fogadott el permissiönöket továbbküldi a db írásra
* R0005 - 2012.06.18. - hibajavítás - a fb set auto grow át lesz helyezve az apiload függvényből az apiinitbe az api betöltése után. a régi megoldással nem működött
* R0006 - 2012.06.22. - hibajavítás - setAutoGrow hibásan működött, az async miatt. az autogrow async be volt míg az init nem. az async el lett emiatt távolítva
* R0007 - 2012.06.24. - ezentúl meg lehet adni az iroda ip címét, így aki az irodából nézi meg az oldalakat, azoknak az adatai nem kerülnek naplózásra a statisztikázóba - első js compresseét tartalom
* R0008 - 2012.07.06. - hiba javítása - apiConectbe a változók neveit módosítani kellett perm-ről perrmissionre, mert így felülírta a függvénybe érkező perm változó tartalmát
* R0009 - 2012.07.17. - új függvény popupInfo néven. Akkor hívódik meg, ha a loadApi-ba paraméterezzük, illetve arról ad tájékoztatást, hogy az előugró ablkaok blokkolva lettek
* R0010 - 2012.07.24. - javítás az alkalmazásban - ha a felhasználó nem vt autentikálva, majd elfoagdta a permissionöket akkor az api hiányos paraméterekkel hívta meg újra a loadApi függvényt
* R0011 - 2012.08.21. - appcenter felügyelete alá kerül a myrtisapi - elérések át lettek írva ennek megfelelően
*/

//Kivétel kezelésére használható függvény
function exception(msg, code) {

}

//flash verzió lekérése
function getFlashVersion(){
	//Internet Explorer
	try {
		try {
			// avoid fp6 minor version lookup issues
			// see: http://blog.deconcept.com/2006/01/11/getvariable-setvariable-crash-internet-explorer-flash-6/
			var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
			try { axo.AllowScriptAccess = 'always'; }
			catch(e) { return '6,0,0'; }
		} catch(e) {

		}
		return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
	//Egyéb böngésző
	} catch(e) {
		try {
			if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){
				return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
			}
		} catch(e) {

		}
	}
	return '0,0,0';
}

//képernyőfelbontás lekérése
function screenRes() {
	return screen.width+' x '+screen.height;
}

var flashver;
var uid;
var accessToken;
var screenres;
var app_id;

/* IE console.log hiba kiküszöbölése
* IE 8 és az alatti verziók esetén a JS elakad a console.log-nál
*/
var alertFallback=false;if(typeof console==="undefined"||typeof console.log==="undefined"){console={};if(alertFallback){console.log=function(msg){alert(msg)}}else{console.log=function(){}}}

// Ha rámennek az alkalmazásra közvetlenül, akkor dobja az erste meghívós oldalára
function loadApiInit(app_id, hover, mode) {
	appid = app_id;
	flashver = getFlashVersion();		//flash verzió lekérése
	screenres = screenRes();			//képernyőfebontás lekérée

    //ha az app éles környezetben van és közvetlenül az oldalról próbálják meg elérni az appot, nem fb-n keresztül, akkor újratölti fb kereten belül
    if (mode == 'prod' && window == window.top)
        window.top.location.href = hover;

	FB.init({
		appId:  app_id,
		cookie: true,
		status: true,
		xfbml:  false,
		oauth:  true,
		channelUrl : 'https://myrtiscreative.hu/channel.html'
	});

	//ablak kihúzása a megfelelő méretre (magasság)
	FB.Canvas.setAutoGrow(300);
}

function loadApi(perm, data) {

	// Megnézzük a login állapotot
	FB.getLoginStatus(function(response) {
		//csatlakoztatva
		if (response.status === 'connected') {

			var permission = '';

			uid = response.authResponse.userID;
			accessToken = response.authResponse.accessToken;

			//lekérjük, hogy az alkalmazások által elkért jogosultságok közül melyek lettek elfogadva, illetve elutasítva
			FB.api('/me/permissions', function(getperms) {
				$.each(getperms.data[0], function(index, value) {
                    permission +=
                        permission === '' ?
                        index + ': ' + value :
                        permission += ', '+index + ': ' + value;
				});
			});

			apiConnected(data);
		//ha nincs autentikálva a felhasználó
		} else if (response.status === 'not_authorized') {
			popupInfo();

            FB.login(function(response) {
				if (response.authResponse)
					// Ha bejelentkezett, akkor api újratöltése
					loadApi(perm, data);
				else
					apiNotAuth(data);
			//Jogosultságok elkérése
			}, {scope: perm});
		//ha nincs bejelentkezve
		} else {
			apiUserNotLogged(data);
		}
	});
}
