(function($){
	var elementPosition;
    
    var pluginMethods = [
        //saját
        ['move', 'setSize', 'setBgColor', 'setTextColor', 'setFontSize', 'setOpacity', 'show', 'hide', 'fadeIn', 'fadeOut', 'fadeToggle', 'slideToggle', 'slideDown', 'slideUp'],
        //ui
        ['blind', 'bounce', 'clip', 'drop', 'explode', 'fade', 'fold', 'highlight', 'puff', 'pulsate', 'shake', 'slide']
    ];

	var methods = {
		// elemek animálása
		animate: function(element, properties, options, delay, type, complete, cDuration) {
			var d = cDuration || options.duration[type];
			setTimeout(function() {
				element.animate(properties, {
                    queue: false, 
                    duration: d, 
                    easing: options.easing[type], 
                    complete: function() {
						if(typeof(options.callback[type]) === 'function') options.callback[type].call(this);
						completeCall(complete, options);
					}
				});
			}, delay);
		},
		
		// elemek mozgatása
		move: function(element, options, complete) {
			var properties = {
				marginTop:  options.top === 0 ? elementPosition.top : typeof(options.top) === 'string' ? options.top : elementPosition.top+options.top,
				marginLeft: options.left === 0 ? elementPosition.top : typeof(options.left) === 'string' ? options.left :  elementPosition.top+options.left
			};
			methods.animate(element, properties, options, options.delay.move, 'move', complete);
		},
		
		// szélesség, magasság állítása
		setSize: function(element, options, complete) {
			var properties = {
				width: options.width === 0 ? elementPosition.width : typeof(options.width) === 'string' ? options.width : elementPosition.top+options.width,
				height: options.height === 0 ? elementPosition.height : typeof(options.height) === 'string' ? options.height : elementPosition.top+options.height
			};
			methods.animate(element, properties, options, options.delay.setSize, 'setSize', complete);
		},
		
		// háttérszín animálása
		setBgColor: function(element, options, complete) {
			setColor(element, options, 'bgColor', options.delay.setBgColor, 'setBgColor', complete);
		},
		
		// szövegszín animálása
		setTextColor: function(element, options, complete) {
			setColor(element, options, 'color', options, 'setTextColor', complete);
		},
		
		// szöveg méretének állítása
		setFontSize: function(element, options, complete) {
			methods.animate(element, {fontSize: options.fontSize === 0 ? options.fontSize : elementPosition.fontSize+options.fontSize}, options, options.delay.setFontSize, 'setFontSize', complete);
		},
		
		// áttetszés állítása
		setOpacity: function(element, options, complete) {
			methods.animate(element, {opacity: parseInt(options.opacity)/100}, options, options.delay.setOpacity, 'setOpacity', complete);
		},
		
		// jQuery UI efektek
		jQueryUI: function(element, options, type, complete) {
			setTimeout(function() {
                if(!element.is(':animated')) {
                    element.effect(type, {easing: options.easing[type]}, options.duration[type], function() {
                        if(typeof(options.callback[type]) === 'function') options.callback[type].call(this);
                        completeCall(complete, options);
                    });
                }
			}, options.delay[type]);
            
		},
		
		show: function(element, options, complete) {
			element.show(options.duration.show, options.easing.show, function() {
				if(typeof(options.callback.show) === 'function') options.callback.show.call(this);
				completeCall(complete, options);
			});
		},
		
		hide: function(element, options, complete) {
			element.hide(options.duration.hide, options.easing.hide, function() {
				if(typeof(options.callback.hide) === 'function') options.callback.hide.call(this);
				completeCall(complete, options);
			});
		},
		
		fadeIn: function(element, options, complete) {
			element.fadeIn(options.duration.fadeIn, options.easing.fadeIn, function() {
				if(typeof(options.callback.fadeIn) === 'function') options.callback.fadeIn.call(this);
                completeCall(complete, options);
			});
		},
		
		fadeOut: function(element, options, complete) {
			element.fadeOut(options.duration.fadeOut, options.easing.fadeOut, function() {
				if(typeof(options.callback.fadeOut) === 'function') options.callback.fadeOut.call(this);
				completeCall(complete, options);
			});
		},
		
		fadeToggle: function(element, options, complete) {
			element.fadeToggle(options.duration.fadeToggle, options.easing.fadeToggle, function() {
				if(typeof(options.callback.fadeToggle) === 'function') options.callback.fadeToggle.call(this);
				completeCall(complete, options);
			});
		},
		
		slideToggle: function(element, options, complete) {
			element.slideToggle(options.duration.slideToggle, options.easing.slideToggle, function() {
				if(typeof(options.callback.slideToggle) === 'function') options.callback.slideToggle.call(this);
				completeCall(complete, options);
			});
		},
		
		slideDown: function(element, options, complete) {
			element.slideDown(options.duration.slideDown, options.easing.slideDown, function() {
				if(typeof(options.callback.slideDown) === 'function') options.callback.slideDown.call(this);
				completeCall(complete, options);
			});
		},
		
		slideUp: function(element, options, complete) {
			element.slideUp(options.duration.slideUp, options.easing.slideUp, function() {
				if(typeof(options.callback.slideUp) === 'function') options.callback.slideUp.call(this);
				completeCall(complete, options);
			});
		}
	};
	
	function completeCall(complete, options) {
		if(complete && typeof(options.complete) === 'function') options.complete.call(this);
	}
	
	function setColor(element, options, type, delay, callback, complete) {
		var count = options[type].length;
		var duration = options.duration[callback]/count;
		methods.animate(
            element, 
            type === 'bgColor' ? {backgroundColor: options[type][0]} : {color: options[type][0]}, 
            options, 
            delay, 
            null, 
            false, 
            duration
        );
		var i = 1;
		var int = setInterval(function(){
			methods.animate(
                element, 
                type === 'bgColor' ? {backgroundColor: options[type][i]} : {color: options[type][i]}, 
                options, 
                delay, 
                i === count-1 ? callback : null, 
                i === count-1 ? complete : false, 
                duration
            );
			i++;
			if(i === count) int = clearInterval(int);
		}, duration);	
	}
	
	$.fn.divAnimate = function(method, options) {
		var element = $(this);
		
        options = $.extend({
			top:        0,
			left:       0,
			width:      0,
			height:     0,
			duration:   {
				move:         400, setSize:      400, setBgColor:   400, setTextColor: 400,
				setFontSize:  400, setOpacity:   400, blind:        400, bounce:       400,
				clip:         400, drop:         400, explode:      400, fade:         400,
				fold:         400, highlight:    400, puff:         400, pulsate:      400,
				shake:        400, slide:        400, show:         400, hide:         400,
				fadeIn:       400, fadeOut:      400, fadeToggle:   400, slideToggle:  400,
				slideDown:    400, slideUp:      400
			},
			easing: {
				move:         'linear', setSize:      'linear', setBgColor:   'linear', setTextColor: 'linear',
				setFontSize:  'linear', setOpacity:   'linear', blind:        'linear', bounce:       'linear',
				clip:         'linear', drop:         'linear', explode:      'linear', fade:         'linear',
				fold:         'linear', highlight:    'linear', puff:         'linear', pulsate:      'linear',
				shake:        'linear', slide:        'linear', show:         'linear', hide:         'linear',
				fadeIn:       'linear', fadeOut:      'linear', fadeToggle:   'linear', slideToggle:  'linear',
				slideDown:    'linear',  slideUp:      'linear'
			},
			bgColor:    [element.css('background-color')],
			color:      [element.css('color')],
			fontSize:   0,
			opacity:    100,
			delay: {
				move:         0, setSize:      0, setBgColor:   0, setTextColor: 0,
				setFontSize:  0, setOpacity:   0, blind:        0, bounce:       0,
				clip:         0, drop:         0, explode:      0, fade:         0,
				fold:         0, highlight:    0, puff:         0, pulsate:      0,
				shake:        0, slide:        0, show:         0, hide:         0,
				fadeIn:       0, fadeOut:      0, fadeToggle:   0, slideToggle:  0,
				slideDown:    0, slideUp:      0
			},
			callback: {}
		}, options);
		
		// div pozíciójának lekérése
		elementPosition = {
			top:      parseInt(element.css('margin-top')),
			left:     parseInt(element.css('margin-left')),
			width:    element.width(),
			height:   element.height(),
			fontSize: parseInt(element.css('font-size'))
		};
				
		// funkció választás
		var func = method.split(', ');
		var count = func.length;
		for(var j = 0, f; f = func[j]; j++) {
            $.each(pluginMethods, function(i, v) {
                $.each(pluginMethods[i], function(index, content) {
                    if(content === f) {
						var complete = j === count-1 ? true : false;
                        switch(i) {
                            case 0: methods[f](element, options, complete); break;
                            case 1: methods.jQueryUI(element, options, f, complete); break;
                            default: console.log('Nem létező metódus: ', f); break;
                        }
                    }
                });
            });
		}
	};
})(jQuery);
