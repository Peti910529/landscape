<?php
/**
 * Statisztikázást és naplózást végez
 *
 * @author Simon Péter <peti@simonnetwork.hu>
 */

namespace Snfapi\CoreBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
/* Entity */
use Snfapi\CoreBundle\Entity\EnvTracking;
use Snfapi\CoreBundle\Entity\AppTracking;

class Statistic {
    private $db;
    private $routing;
    private $param;
	private $env;
	private $fb_uid;
	private $server;
	private $em;

	/**
	 * @param mixed[] $service_container
	 */
    function __construct($service_container) {
        //rouingot tárolja
        $this->routing =    $service_container->get('request')->get('_route');
        //konfigurációs paramétereket tárol
        $this->param =      $service_container->parameters;
		$this->db =         $service_container->get('database_connection');
		$this->env =		$service_container->getParameter('kernel.environment');
		$this->fb_uid = 	$service_container->get('core_facebook')->getUser();
		$this->server =		$service_container->get('request')->server;
		$this->em =			$service_container->get('doctrine')->getManager();
    }

    /*
     * aktív statisztikázást végez
     *
     * naplózza, hogy melyik felhasználó mikor hol tartózkodott az alkalmazáson belül
	 *
	 * @return void
     */
    function activeStatistic() {
		$app_tracking = new AppTracking;
		$app_tracking->setFbUid($this->fb_uid);
		$app_tracking->setModule($this->routing);
		
		$this->em->persist($app_tracking);
		$this->em->flush();
    }
    
    /**
     * környezet statisztikázása
     * 
     * statisztikázza a bejelentkezett felhasználó szoftverkörnyezetét
	 *
	 * @return void
     */
    function envStat() {
		$env_tracking = new EnvTracking;
		$env_tracking->setFbUid($this->fb_uid);
		$env_tracking->setUserAgent($this->server->get('HTTP_USER_AGENT'));
		$env_tracking->setIp($this->server->get('REMOTE_ADDR'));
		
		$this->em->persist($env_tracking);
		$this->em->flush();
    }
    
    /**
     * ha a php kód futása befejeződött, meghvíja a modulstaisztikázót
	 *
	 * @param Event $event
	 * @return void
     */
    public function onController ($event) {
		$this->activeStatistic();
		
		//ha az index routing-ból próbáljuk elérni, akkor futtassa le a szoftverkörnyezet statisztikázását
		if(preg_match('/^(index)$/', $this->routing))
			$this->envStat();
    }
}
