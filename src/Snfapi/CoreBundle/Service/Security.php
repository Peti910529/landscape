<?php
/**
 * Biztonsági funkciókat tartalmazó osztály
 *
 * @author Simon Péter <peti@simonnetwork.hu>
 */

namespace Snfapi\CoreBundle\Service;

class Security {
    private $session;

	/**
	 * @param mixed[] $service_container
	 */
    final public function __construct($service_container) {
        //lekérjük a szolgáltatás konténerből a session tartalmát
        $this->session  = $service_container->get('session');
    }

    /**
	 * Feketelista - ellenőrzi, hogy a saját id szerepel-e a feketelistán
	 *
	 * @return function|bool
	 */
	final public function blackList($id = false) {
        if($id !== false) {
            if(is_array($id)) {
                foreach($id as $fb_uid) {
                    return $this->blascklistEquivalent($fb_uid);
                }
            } else {
                return $this->blascklistEquivalent($id);
            }
        }

        return false;
    }

    /**
	 * Ellenőrzi, hogy a saját id szerepel-e a feketelistán
     *
	 * @param int $id
	 * @return bool
	 */
	final private function blascklistEquivalent($id) {
        if($this->session->get('fb_id') === $id) {
            return true;
        }
        return false;
    }
}
