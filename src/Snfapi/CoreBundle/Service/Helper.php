<?php
/**
 * Segítő osztályok
 *
 * @author Simon Péter
 */

namespace Snfapi\CoreBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;

class Helper {
    private $container;
	private $env;
    
	/**
	 * @param mixed[] $service_container
	 */
    function __construct($service_container) {
        $this->container =  $service_container;
        $this->param =      $service_container->parameters;
		$this->env =		$service_container->getParameter('kernel.environment');
    }
    
    /**
	 * Ellenőrzi a kapcsolat típusát, hogy http vagy https
	 *
	 * @param string $url
	 */
    function isHttps($url = '') {
		//ha nem https, töltse újra az alkalmazást https-el
        isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on" ? $prot = 'https' : $prot = 'http';

        //ha a protokol http
        if ($prot == 'http') {
			if($url == '') {
				die('<script type="text/javascript">window.top.location.href = \''.$this->param['myrtis_main_application']['facebook']['app_link'].'\'</script>');
			}
		}
    }

    /*
     * Hiba kiiratása
     *
	 * @param mixed[] $var
     * @param bool $stop Megállítható a kód futtatása
     * @return void
	 */
    static function dump($var, $stop = true) {
        echo '<pre>'; var_dump($var);
        if($stop)
            die('</pre>');
        else
            echo '</pre>';
    }
	
	function onController ($event) {
		//csak prod módban fut le
		if($this->env == 'prod')
			$this->get('core_helper')->isHttps();
    }
}
