<?php
/**
 * Kivételek kezelése
 *
 * @author Simon Péter
 */

namespace Snfapi\CoreBundle\Service;

class SnfapiException extends \Exception {
    final public function __construct($msg = 'Hiba lépett fel!', $code = 0) {
        parent::__construct($msg, $code);
    }
}
