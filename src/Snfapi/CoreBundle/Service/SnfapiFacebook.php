<?php
/**
 * Description of SnfapiFacebook
 *
 * @author Simon Péter <peti@simonnetwork.hu>
 */

namespace Snfapi\CoreBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;

final class SnfapiFacebook extends \Facebook {
    private $session;
    private $logger;
    private $param;
	private $all_param;
    private $routing;
	private $request;
	private $env;
	private $container;

	/**
	 * @param mixed[] $service_container
	 */
    function __construct($service_container) {
		$this->container =	$service_container;
		$this->request =	$service_container->get('request');
        $this->routing =    $service_container->get('request')->get('_route');
        $this->session =    $service_container->get('session');
        $this->logger =     $service_container->get('logger');
        $this->param =      $service_container->parameters['snfapi_main_application']['facebook'];
		$this->all_param =  $service_container->parameters;
		$this->env =		$service_container->getParameter('kernel.environment');
		$this->db =			$service_container->get('database_connection');
        $this->config();
    }
    
    /**
	 * Felhasználó bejelentkezése Facebook API-ba
     *
	 * @return int|void
	 */
    function login() {
		//ha az alkalmazás demo módban fut, akkor a felhasználó azonosítója 1 lesz
		if($this->param['mode'] == 'demo') return 1;
		
		$fb_uid = $this->getUser();
		
        //ha nem érvényes az access token vagy nincs bejelentkezve a felhasználó, és nem az index oldalon tartózkodik -> facebook bejelentkeztetés
        if((!$this->checkAccessToken() || $fb_uid == 0) && $this->routing != 'index') {
			//redirect link
			$link = $this->getLoginUrl(
				array(
					'scope'         => $this->param['perms'],
					'redirect_uri'  => $this->param['redirect_uri'].$this->routing,
				)
			);
		
			die('<script type="text/javascript">window.top.location.href = \''.$link.'\'</script>');
		//ha már bejelentkezett visszatérünk a facebbok azonosítóval
        } else
			return $fb_uid;
    }
	
	/**
	 * Kényszerített Facebook bejelentkezés - a hiányzó jogokat elkéri a felhasználótól.
	 * Hasznos például olyan esetben lefuttatni, ha egy új jogosultságot adtunk hozzá
	 *
	 * @return void
	 */
	function forceLogin() {
		//ellenőrzi, hogy az adoott felhasználó be van-e jeletkezve
        $link = $this->getLoginUrl(
            array(
                'scope'         => $this->param['perms'],
                'redirect_uri'  => $this->param['redirect_uri'].$this->routing,
            )
        );

		die('<script type="text/javascript">window.top.location.href = \''.$link.'\'</script>');
	}

    /**
	 * Facebook api bekonfigurálása
     *
	 * @return void
	 */
	function config() {
        parent::__construct(array(
            'appId' => $this->param['app_id'],
            'secret' => $this->param['app_secret'],
            'fileUpload' => true,
        ));
    }
	
	/**
	 * Ellenőrzi, hogy érvényes-e az access token
	 *
	 * @return bool
	 */
	private function checkAccessToken() {
		//Graph API kérés összeállítása
		
		$graph_url = 'https://graph.facebook.com/me?access_token='.$this->getAccessToken();
		$response = $this->curl_get_file_contents($graph_url);
		$decoded_response = json_decode($response);
    
		//hiba ellenőrzése
		if (isset($decoded_response->error)) {
			//ha az access token lejárt és amiatt nem lehet lekérni érvényes token-t
			if ($decoded_response->error->type== "OAuthException")
				$error_code = $decoded_response->error->code;
				
				switch($error_code) {
					case 2:
						$error = 'FacebookApiException - '.__FILE__.' - Line: '.__LINE__.' - (Invalid Access Token)';
						$this->logger->err($error);
						throw new \Exception($error);
					default: return false;
				}
				
				return false;
		} else
			return true;
	}
	
	/**
	 * CURL kérés
	 *
	 * note this wrapper function exists in order to circumvent PHP’s 
	 * strict obeying of HTTP error codes.  In this case, Facebook 
	 * returns error code 400 which PHP obeys and wipes out 
	 * the response.
	 *
	 * @param string $url
	 * @return string|bool
	 */
	private function curl_get_file_contents($URL) {
		$c = curl_init();
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $URL);
		$contents = curl_exec($c);
		$err  = curl_getinfo($c, CURLINFO_HTTP_CODE);
		curl_close($c);
		return $contents ? $contents : false;
	}

    /**
	 * Üzenőfalra posztolás - adatok összeállítása
     *
	 * @param mixed[] $param
	 * @param string $type_id
	 * @param int $id
	 * @return void
	 */
	private function post($param, $type, $id) {
		/**
		 * lekérjük, és hozzáfűzzük az access token értéket
		 *
		 * ennek hiányában elképzelhető, hogy megosztás / képfeltöltés nem fog megfelelően működni. - leggyakoribb hiba: a felhazsnáló nincs autentikálva
		 */
		$param['access_token'] = $this->getAccessToken();
		
        //ha nem adott meg id-t akkor saját magunknak posztol
        if($id === 'me')
            $this->poster($param, $type, $id);
        //ha megadott id-t
        else {
            //ha az id tömb, akkor több személynek kell kiküldeni
            if(is_array($id)) {
                foreach($id as $fb_id) {
                    $this->poster($param, $type, $fb_id);
                }
            //ha nem tömb akkor csak az adott azonosítóra
            } else
                $this->poster($param, $type, $id);
        }
    }

    //üzenőfalra posztolás - ténylegesen itt posztolja ki az ünefalra a tartalmat
    private function poster($param, $type, $id) {
		try {
			$this->api('/'.$id.'/'.$type, 'post', $param);
		} catch(\Exception $e) {
			$this->logger->err('FacebookApiException - '.__FILE__.' - Line: '.__LINE__.' - ('.$e->getCode().')'.$e->getMessage());
		}
    }

    //szöveges tartalom üzenőfalra posztolása
    function wallPost($param, $id = 'me') {
        $this->post($param, 'feed', $id);
    }

    //kép posztolása üzenőfalra
    function imgPost($param, $id = 'me') {
        try {
            $this->post($param, 'photos', $id);
        } catch(FacebookApiException $e) {
             $this->logger->err('FacebookApiException - '.__FILE__.' - Line: '.__LINE__.' - ('.$e->getCode().')'.$e->getMessage());
            }
        }

	/**
	 * ellenőrzi, hogy az oldalt lájkolták-e
	 * 
	 * csak akkor jön be a figyelmeztetés, ha a symfony kernel prod módban fut
	 * 
	 * true - az oldal lájkolva van
	 * false - az oldal nincs lájkolva
	 *
	 * @param mixed[] $signed_request
	 * @return void
	 */
    function isFan($signed_request) {
		//ha a lájk kapu aktív és az alkalmazás prod módban fut
		if($this->param['like_gate'] && $this->env == 'prod')
			//visszatér, hogy lájkolták-e
			return @$signed_request['page']['liked'] ? true : false;
		//ha dev módban fut, visszatér azzal, hogy lájkolták az oldalt - emiatt a fejlesztés alatt nem fog megjelenni a popup
		else
			return true;
    }

    /**
	 * Ellenőrzi, hogy az összes Facebook jogosultságot elfogadta-e a felhasználó
	 * 
	 * @return bool
	 */
    function permissionCheck() {
		$permissions = $this->api('/me/permissions', 'GET');
		$permissions = $permissions['data'][0];
		
		$require_permissions = $container = $this->param['perms'];
		$erp = explode(',', $require_permissions);
		
		$result = true;
		
		foreach($erp as $permission) {
			if(!array_key_exists(trim($permission), $permissions)) {
				$result = false;
				continue;
			}
		}
		
		return $result;
    }
	
	/**
	 * ellenőrzi, hogy a felhasználó elfogadta-e az összes jogosultságot, amit a facebook bekért
	 *
	 * @return void
	 */
	function checkPermissions() {
		$config_perms	= explode(',', $this->param['perms']);
		$perms			= $this->getPermissions();
		
		foreach($config_perms as $c_perm) {
			trim($c_perm);	//eltávolítjuk a whitespaceket, ha esetleg a konfigurációs fájlba tettünk szóközt a vessző után
		}
	}	
	
	/**
	 * Ellenőrzi, majd facebook bejelentkeztetés után átirányítja a felhasználót a kért oldalra.
	 * Ez olyan esetben előnyös, ha például az alkalmazás böngészése közben lejár a Facebook Access Token
	 *
	 * @param string $page
	 * @return void
	 */
	function requestPage($page) {
		//ha most jelentkezett be az alkalmazásba vagy a kérés nem ajax kérés
        if($this->request->get('state') && $this->request->get('code') || !isset ($_SERVER['HTTP_X_REQUESTED_WITH']) && $page != 'landing') {
			$query =	$this->request->query->all();
			$request =	$this->request->request->all();
		
			$final_request = '';
		
			foreach($query as $key => $value) {
				$final_request .= $key.'='.$value.'&';
			}
			
			foreach($request as $key => $value) {
				$final_request .= $key.'='.$value.'&';
			}
			
			$this->db->insert('fb_app_login_control', array(
				'fb_uid'	=> $this->login(),
				'page'		=> $page,
				'data'		=> $final_request
			));
			
			$link = isset($this->param['app_link']) ? $this->param['app_link'] : $this->param['redirect_uri'].'landing';
			die('<script type="text/javascript">window.top.location.href = \''.$link.'\'</script>');
        }
	}
	
	/**
	 * @param string $page
	 * @return mixed
	 */
	function loadPage($page) {
		$fb_uid = $this->login();
	
		$stmt = $this->db->prepare('
			SELECT page, data
			FROM fb_app_login_control
			WHERE
				fb_uid = :fb_uid
		');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->execute();
		$result = $stmt->fetch();
		
		//ha a felhasználó újra be lett jelentkeztetve és nem a landing page-re érkezik
		if($result) {
			//töröljük a bejelentkeztetés adatait, hogy leközelebb ne irányítson át az alkalmazás, ha a felhasználó a landign page-re ér
			$this->db->delete('fb_app_login_control', array(
				'fb_uid' => $fb_uid
			));
			
			return array(
				'page'	=> $result['page'],
				'data'	=> $result['data'],
			);
		} else {
			return array(
				'page'	=> $page,
				'data'	=> '',
			);
		}
	}
}
