<?php
/*
 * @author Simon Péter
 */
namespace Snfapi\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Response,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ErrorController extends Controller
{
    /**
     * @Route("/http/{code}", name="httpresp")
     * @Template()
     */
    public function httprespAction ($code)
    {
        switch ($code)
        {
            case 400: $text = 'Érvénytelen kérés.';              break;
            case 401: $text = 'Nem autentikált.';                break;
            case 403: $text = 'Hozzáférés megtagadva.';          break;
            case 404: $text = 'A keresett oldal nem található.'; break;
            case 405: $text = 'Metódus nincs engedélyezve.';     break;
            case 500: $text = 'Szerverhiba.';                    break;
            case 502: $text = 'Hibás átjáró.';                   break;
            case 503: $text = 'Karbantartás miatt szünetel.';    break;
        }

        $response = new Response();
        $response->setStatusCode ($code);
        $response->send();

        return compact ('code', 'text');
    }
}
