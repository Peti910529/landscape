<?php

namespace Snfapi\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FbAppLoginControl
 *
 * @ORM\Table(name="fb_app_login_control")
 * @ORM\Entity
 */
class FbAppLoginControl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=64, nullable=false)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=false)
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var \Snfapi\CoreBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Snfapi\CoreBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fb_uid", referencedColumnName="fb_uid")
     * })
     */
    private $fbUid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return FbAppLoginControl
     */
    public function setPage($page)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return FbAppLoginControl
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return FbAppLoginControl
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fbUid
     *
     * @param \Snfapi\CoreBundle\Entity\Users $fbUid
     * @return FbAppLoginControl
     */
    public function setFbUid(\Snfapi\CoreBundle\Entity\Users $fbUid = null)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }

    /**
     * Get fbUid
     *
     * @return \Snfapi\CoreBundle\Entity\Users 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }
}