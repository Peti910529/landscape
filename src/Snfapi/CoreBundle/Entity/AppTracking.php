<?php

namespace Snfapi\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppTracking
 *
 * @ORM\Table(name="app_tracking")
 * @ORM\Entity
 */
class AppTracking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_uid", type="bigint", nullable=false)
     */
    private $fbUid;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=64, nullable=false)
     */
    private $module;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fbUid
     *
     * @param integer $fbUid
     * @return AppTracking
     */
    public function setFbUid($fbUid)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }

    /**
     * Get fbUid
     *
     * @return integer 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return AppTracking
     */
    public function setModule($module)
    {
        $this->module = $module;
    
        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return AppTracking
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}