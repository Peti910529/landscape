<?php

namespace Snfapi\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnvTracking
 *
 * @ORM\Table(name="env_tracking")
 * @ORM\Entity
 */
class EnvTracking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_uid", type="bigint", nullable=false)
     */
    private $fbUid;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="text", nullable=false)
     */
    private $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=32, nullable=false)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set id
     *
     * @param integer $id
     * @return EnvTracking
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }
	
    /**
     * Set fbUid
     *
     * @param integer $fbUid
     * @return EnvTracking
     */
    public function setFbUid($fbUid)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }

    /**
     * Get fbUid
     *
     * @return integer 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return EnvTracking
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    
        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string 
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return EnvTracking
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return EnvTracking
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}