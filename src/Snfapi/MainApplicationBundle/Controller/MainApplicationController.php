<?php
namespace Snfapi\MainApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\RedirectResponse,
	Symfony\Component\Security\Core\Util\SecureRandom,
	Symfony\Component\DependencyInjection\Exception\RuntimeException,
	
    /* Symfony annotation */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache,
        
    /* SnfApi Core */    
    Snfapi\CoreBundle\Service\Helper,
    Snfapi\CoreBundle\Service\MyrtisFacebook,
    Snfapi\CoreBundle\Service\Config,
	
	/* Entity */
	Snfapi\MainApplicationBundle\Entity\Users,
	Snfapi\MainApplicationBundle\Entity\ImageUpload,
	Snfapi\MainApplicationBundle\Entity\Comment
;

class MainApplicationController extends Controller {
    
    /**
     * @Route("/{page}", name="index", defaults={"page" = "landing"})
	 * @Template
     */
    function indexAction(Request $request, $page) {
		/* Snfapi Core */
        $main_app =		$this->container->parameters['snfapi_main_application'];
        $facebook =		$this->get('core_facebook');
		$load =			$facebook->loadPage($page);
		$facebook->requestPage($page);

        return array(
			'page'          => $load['page'],
			'dataquery'     => $load['data'],
			'fan_popup'     => $facebook->isFan($facebook->getSignedRequest()),
			'app_id'        => $main_app['facebook']['app_id'],
			'app_link'      => $main_app['facebook']['app_link']
        );
    }
    
    /**
     * @Cache(expires="+7 days")
     * @Route("/landing/", name="landing")
     * @Template
     */
    function landingAction(Request $request) {
		$video_id = array('wqKv9-ADi9k', 'F52BgNPENh8');
	
        return array(
			'video_id'	=> $video_id[mt_rand(0, count($video_id)-1)]
		);
    }
	
	/**
	 * beirányítja a facebook felhasználót az alkalmazásba - hasznos, ha a felhasználó hírdetésből jött
	 * 
	 * @Route("/index/", name="mainindex")
	 */
	function mainIndexAction() {
		$facebook_config = $this->container->parameters['snfapi_main_application']['facebook'];
		$link = isset($facebook_config['app_link']) ? $facebook_config['app_link'] : $facebook_config['redirect_uri'].'landing';
		return new RedirectResponse($link);
	}
	
	/**
	 * Pályázás felület
	 *
	 * @Route("/welcome/", name="welcome")
	 * @Template
	 */
	function welcomeAction(Request $request) {
		$facebook	= $this->get('core_facebook');
		$fb_uid		= $facebook->login();
		$conn		= $this->get('database_connection');
		
		//ha nem fogadta volna el a felhasználó az összes jogosultságot
		if(!$facebook->permissionCheck()) {
			$facebook->forceLogin();
		}
		
		$stmt = $conn->prepare('SELECT COUNT(id) AS sum FROM users WHERE fb_uid = :fb_uid');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->execute();
		$result = $stmt->fetch();
		
		//ha a felhasználó még nem regisztrált
		if($result['sum'] == 0)
			return $this->redirect($this->generateUrl('registration'));
		
		$image_upload = new ImageUpload;
		
		$form = $this->createFormBuilder($image_upload)
            ->add('name', 'email', array('attr' => array('placeholder' => 'Név')))
            ->add('image', 'file', array('attr' => array('accept' => 'image/jpeg, image/png')))
        ->getForm();
		
		if(!is_null($this->getRequest()->request->get('form'))) {
            $form->bind($this->getRequest());
            if($form->isValid()) {
				$data = $form->getData();
                $conn = $this->get('database_connection');
				
				$generator	= new SecureRandom();
				
				if(!is_null($image_upload->image)) {
					$file		= bin2hex($generator->nextBytes(16));
					$extension	= $image_upload->getExtension();
					
					$image_upload->upload($file, $extension);
				
					$conn->insert('images', array(
						'fb_uid'	=> $fb_uid,
						'name'		=> $data->name,
						'filename'	=> $file.'.jpg'
					));
				}
			}
		}
		
		return array(
			'form'	=> $form->createView()
		);
	}
	
	/**
	 * Regisztrációs felület
	 *
	 * @Route("/registration/", name="registration")
	 * @Template
	 */
	function registrationAction(Request $request) {
		$facebook	= $this->get('core_facebook');
		$fb_uid		= $facebook->login();
		$user_data	= $facebook->api('/me', 'GET');
		
		$form = $this->createFormBuilder(new Users)
            ->add('email', 'email', array('attr' => array('placeholder' => 'E-mail', 'value' => $user_data['email'])))
            ->add('name', 'text', array('attr' => array('placeholder' => 'Név', 'value' => $user_data['name'])))
            ->add('news', 'checkbox', array('attr' => array()))
        ->getForm();
		
		if(!is_null($this->getRequest()->request->get('form'))) {
            $form->bind($this->getRequest());
            if($form->isValid()) {
                $data = $form->getData();
                $conn = $this->get('database_connection');
				
				$conn->insert('users', array(
					'fb_uid'	=> $fb_uid,
					'email'		=> $data->email,
					'name'		=> $data->name,
					'news'		=> (int)$data->news
				));
				
				return $this->redirect($this->generateUrl('welcome'));
			}
		}
		
		return array(
			'form'	=> $form->createView()
		);
	}
	
	/**
	 * Profil oldal
	 *
	 * @Route("/profile/", name="profile")
	 * @Template
	 */
	function profileAction(Request $request) {
		$facebook	= $this->get('core_facebook');
		$fb_uid		= $facebook->login();
		$conn 		= $this->get('database_connection');
		
		$stmt = $conn->prepare('
			SELECT email, name, news FROM users WHERE fb_uid = :fb_uid
		');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->execute();
		$user_data = $stmt->fetch();
		
		$form = $this->createFormBuilder(new Users)
            ->add('email', 'email', array('attr' => array('placeholder' => 'E-mail', 'value' => $user_data['email'])))
            ->add('name', 'text', array('attr' => array('placeholder' => 'Név', 'value' => $user_data['name'])))
            ->add('news', 'checkbox', array('data' => $user_data['news'] == 1 ? true : false))
        ->getForm();
		
		if(!is_null($this->getRequest()->request->get('form'))) {
            $form->bind($this->getRequest());
            if($form->isValid()) {
                $data = $form->getData();
				
				$conn->update('users', array(
					'email'		=> $data->email,
					'name'		=> $data->name,
					'news'		=> (int)$data->news
				), array(
					'fb_uid'	=> $fb_uid
				));
				
				return $this->redirect($this->generateUrl('profile'));
			}
		}
		
		return array(
			'form'	=> $form->createView()
		);
	}
	
	/**
	 * Képek és szavazás
	 *
	 * @Route("/images/", name="images")
	 * @Template
	 */
	function imagesAction(Request $request) {
		$facebook 	= $this->get('core_facebook');
		$fb_uid 	= $facebook->login();
		$conn		= $this->get('database_connection');
		$page		= is_null($request->get('page')) ? 1 : $request->get('page');
		
		$count = $conn->prepare('SELECT COUNT(id) AS sum FROM images');
		$count->execute();
		$sum = $count->fetch();

		$step		= 6;
		$start		= ($step * $page) - $step;
		$all		= ceil($sum['sum']/$step);
		
		if(!$all) {
			$all = 1;
		}
		
		if($page < 1) {
			return $this->redirect($this->generateUrl('images'));
		}
		
		if($all < $page) {
			return $this->redirect($this->generateUrl('images', array('page' => $all)));
		}
		
		$stmt = $conn->prepare('
			SELECT
				id, fb_uid, name, filename,
				(SELECT COUNT(id) FROM votes WHERE image_id = images.id) AS all_vote,
				(SELECT COUNT(id) FROM votes WHERE image_id = images.id AND fb_uid = :fb_uid) AS my_vote
			FROM images
			ORDER BY id DESC
			LIMIT '.$start.', '.$step.'
		');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->execute();
		
		return array(
			'images'	=> $stmt->fetchAll(),
			'fb_uid'	=> $fb_uid,
			'all'		=> $all,
			'page'		=> $page,
			'active'	=> 'images'
		);
	}
	
	/**
	 * Saját képek megjelenítése
	 *
	 * @Route("/myimages/", name="myimages")
	 * @Template("SnfapiMainApplicationBundle:MainApplication:images.html.twig")
	 */
	function myimagesAction(Request $request) {
		$facebook 	= $this->get('core_facebook');
		$fb_uid 	= $facebook->login();
		$conn		= $this->get('database_connection');
		$page		= is_null($request->get('page')) ? 1 : $request->get('page');
		
		$count = $conn->prepare('SELECT COUNT(id) AS sum FROM images WHERE fb_uid = :fb_uid');
		$count->bindParam('fb_uid', $fb_uid);
		$count->execute();
		$sum = $count->fetch();

		$step		= 6;
		$start		= ($step * $page) - $step;
		$all		= ceil($sum['sum']/$step);
		
		if(!$all) {
			$all = 1;
		}
		
		if($page < 1) {
			return $this->redirect($this->generateUrl('myimages'));
		}
		
		if($all < $page) {
			return $this->redirect($this->generateUrl('myimages', array('page' => $all)));
		}
		
		$stmt = $conn->prepare('
			SELECT
				id, fb_uid, name, filename,
				(SELECT COUNT(id) FROM votes WHERE image_id = images.id) AS all_vote
			FROM images
			WHERE fb_uid = :fb_uid
			ORDER BY id DESC
			LIMIT '.$start.', '.$step.'
		');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->execute();
		
		return array(
			'images'	=> $stmt->fetchAll(),
			'fb_uid'	=> $fb_uid,
			'all'		=> $all,
			'page'		=> $page,
			'active'	=> 'myimages'
		);
	}
	
	/**
	 * Szavazás
	 * 
	 * @Route("/vote/{id}", name="vote")
	 * @Template
	 */
	function voteAction($id, Request $request) {
		$facebook 	= $this->get('core_facebook');
		$fb_uid 	= $facebook->login();
		$conn		= $this->get('database_connection');
		
		$stmt = $conn->prepare('
			REPLACE INTO votes (
				fb_uid,
				image_id
			) VALUES (
				:fb_uid,
				:image_id
			)
		');
		$stmt->bindParam('fb_uid', $fb_uid);
		$stmt->bindParam('image_id', $id);
		$stmt->execute();
		
		return $this->redirect($this->generateUrl('images', array(
			'page'	=> $request->get('page')
		)));
	}
	
	/**
	 * Kép törlése
	 *
	 * @Route("/imagedel/", name="imagedel")
	 * @Template
	 */
	function imagedelAction(Request $request) {
		$facebook 	= $this->get('core_facebook');
		$fb_uid 	= $facebook->login();
		$id			= $request->get('id');
		
		$session = $this->get('session');
		
		if($request->get('type') == 1) {
			return compact('id');
		} else {
			$csrf_provider = $this->get('form.csrf_provider');
			
			if (!$csrf_provider->isCsrfTokenValid('token', $request->get('token')))
				throw new RuntimeException('CSRF attack detected.');
				
			$conn = $this->get('database_connection');
			$conn->delete('images', array(
				'id'	=> $id
			));
			
			return $this->redirect($this->generateUrl('images', array(
				
			)));
		}
	}
	
	/**
	 * Menüsor
	 *
	 * @Route("/nav/", name="nav")
	 * @Template
	 */
	function navAction(Request $request) {
		$facebook	= $this->get('core_facebook');
		$fb_uid		= $facebook->login();
		
		return array(
			'fb_uid'	=> $fb_uid,
			'active'	=> $request->get('active')
		);
	}
	
	/**
	 * Nagyméretű kép megtekintése
	 *
	 * @Route("/image/{id}", name="image")
	 * @Template
	 */
	function imageAction($id, Request $request) {
		$facebook	= $this->get('core_facebook');
		$fb_uid		= $facebook->login();
		$conn		= $this->get('database_connection');
		
		$comment = new Comment;
		
		$form = $this->createFormBuilder($comment)
            ->add('content', 'textarea', array('attr' => array('placeholder' => 'Megjegyzés')))
        ->getForm();
		
		if(!is_null($this->getRequest()->request->get('form'))) {
            $form->bind($this->getRequest());
            if($form->isValid()) {
				$data = $form->getData();
				
				$conn->insert('comments', array(
					'fb_uid'	=> $fb_uid,
					'image_id'	=> $id,
					'content'	=> $data->content
				));
			}
		}
		
		$stmt = $conn->prepare('
			SELECT c.id AS id, c.fb_uid AS fb_uid, name, content, c.date AS date
			FROM comments c
			INNER JOIN users u ON
				c.fb_uid = u.fb_uid
			WHERE image_id = :id
			ORDER BY c.date DESC
		');
		$stmt->bindParam('id', $id);
		$stmt->execute();
		
		$image = $conn->prepare('
			SELECT
				name,
				filename,
				(SELECT COUNT(id) FROM votes WHERE image_id = :id) AS all_vote,
				(SELECT COUNT(id) FROM votes WHERE image_id = :id AND fb_uid = :fb_uid) AS my_vote
			FROM images
			WHERE id = :id
		');
		$image->bindParam('id', $id);
		$image->bindParam('fb_uid', $fb_uid);
		$image->execute();

		$prev = $conn->prepare('SELECT id FROM `images` WHERE id > :id LIMIT 1');
		$prev->bindParam('id', $id);
		$prev->execute();

		$next = $conn->prepare('SELECT id FROM `images` WHERE id < :id ORDER BY id DESC LIMIT 1');
		$next->bindParam('id', $id);
		$next->execute();
		
		return array(
			'image_id'	=> $id,
			'comments'	=> $stmt->fetchAll(),
			'form' 		=> $form->createView(),
			'image' 	=> $image->fetch(),
			'fb_uid'	=> $fb_uid,
			'prev'		=> $prev->fetch(),
			'next'		=> $next->fetch()
		);
	}
	
	/**
	 * Kép kommentár törlése
	 *
	 * @Route("/commentdel/", name="commentdel")
	 * @Template
	 */
	function commentdelAction(Request $request) {
		$facebook 	= $this->get('core_facebook');
		$fb_uid 	= $facebook->login();
		$id			= $request->get('id');
		
		$session = $this->get('session');
		
		if($request->get('type') == 1) {
			return array(
				'image_id'	=> $request->get('image_id'),
				'id'		=> $id
			);
		} else {
			$csrf_provider = $this->get('form.csrf_provider');
			
			if (!$csrf_provider->isCsrfTokenValid('token', $request->get('token')))
				throw new RuntimeException('CSRF attack detected.');
				
			$conn = $this->get('database_connection');
			$conn->delete('comments', array(
				'id'	=> $id
			));
			
			return $this->redirect($this->generateUrl('image', array(
				'id'	=> $request->get('image_id')
			)));
		}
	}
}
