<?php
namespace Snfapi\MainApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request,
	Symfony\Component\HttpFoundation\Response,
    Symfony\Component\Form\FormError,
	Symfony\Component\Security\Core\Util\SecureRandom,
        
    /* Symfony annotation */
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache,
        
    /* SnfApi Core */    
    Snfapi\CoreBundle\Service\Helper,
    Snfapi\CoreBundle\Service\MyrtisFacebook,
    Snfapi\CoreBundle\Service\Config,
	
	/* Entity */
	Snfapi\MainApplicationBundle\Entity\Users,
	Snfapi\MainApplicationBundle\Entity\ImageUpload
;

class MobileApplicationController extends Controller {
	
    /**
	 * mobil felület - ellenőrzi, hogy az adott héten vettek-e már részt képfeltöltésben
	 * 
     * @Route("/mobileisregistered/", name="mobileisregistered")
     * @Template
     */
    function mobileisregisteredAction(Request $request) {
        $fb_uid = $request->get('fb_uid');
        $conn = $this->get('database_connection');
		
        //lekérjük a csapatban szerepelő játékosok azomosítóját
        $stmt = $conn->prepare('
            SELECT COUNT(id) AS sum
            FROM users
            WHERE fb_uid = :fb_uid
        ');
        $stmt->bindParam('fb_uid', $fb_uid);
        $stmt->execute();
        $result = $stmt->fetch();
		$registered = $result['sum'] == 0 ? false : true;
        
        $response = new Response(json_encode(array('result' => $registered)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
	
    /**
     * kép feltöltése
	 *
	 * @Route("/mobileimgupload/", name="mobileimgupload")
     * @Template
     */
    function mobileimguploadAction(Request $request) {
        $fb_uid		= $request->get('fb_uid');
		$conn		= $this->get('database_connection');
        
        $logger = $this->get('logger');
        $logger->info($request->get('image'));
        $logger->info($request->get('fb_uid'));
        
		$generator	= new SecureRandom();
				
		//fájl neve - random string + kiterjesztés - ha a fájl létezik
        $filename = bin2hex($generator->nextBytes(16)).'.jpeg';
		
        //beírjuk a képet a helyére
        $conn->insert('images', array(
            'fb_uid'    => $fb_uid,
			'name'		=> 'Tájkép',
			'filename'	=> $filename
        ));

        $binary = base64_decode($request->get('image'));

        //végleges fájl mentése
        $file = fopen('uploads/images/'.$filename, 'wb');
        fwrite($file, $binary);
        fclose($file);
        
        $response = new Response(json_encode(array('success' => true)));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
	
    /**
	 * Szavazás felület
	 *
     * @Route("/mobileimages/", name="mobileimages")
     * @Template
     */
    function mobileimagesAction(Request $request) {
		$conn = $this->get('database_connection');
        $stmt = $conn->prepare('SELECT id, filename FROM images ORDER BY RAND() LIMIT 10');
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $response = new Response(json_encode(array('images' => $result)));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
	
	/**
	 * Szavazás
	 *
     * @Route("/mobilevote/", name="mobilevote")
     * @Template
     */
    function mobilevoteAction(Request $request) {
        $img_id = $request->get('img_id');
        $fb_uid = $request->get('fb_uid');
		$conn	= $this->get('database_connection');
        
        //ellenőrzi, hogy szavaztál-e már a képre
        $stmt = $conn->prepare('
            SELECT COUNT(id) AS sum
            FROM votes
            WHERE
                fb_uid = :fb_uid AND
                image_id = :image_id
        ');
        $stmt->bindParam('image_id', $img_id);
        $stmt->bindParam('fb_uid', $fb_uid);
        $stmt->execute();
        $count = $stmt->fetch();
        
        //ha igen
        if($count['sum'] != 0) {
            $response = new Response(json_encode(array(
                'success' => false,
                'msg' => 'Már szavaztál erre a képre!' 
            )));
        //ha nem
        } else {
            $conn->insert('votes', array(
				'fb_uid' 	=> $fb_uid,
				'image_id' 	=> $img_id
            ));
            
            $response = new Response(json_encode(array(
                'success' => true,
                'msg' => 'Szavazatodat rögzítettük!', 
            )));
            
        }
        
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
