$(function () {
    var container = $('#container');

    loadApiInit(217478378369588, '', 'dev');
    
    container
        .ajaxload ({
            url_query   : container.data ('page'),
            dataType    : 'html',
            method      : 'post',
            cache       : false,
            pageStatus  : {
                200: function() { loadComponent(); },
                503: function() {
                    alert('Az alkalmazás karbantartás miatt szünetel!');
                }
            },
            noNetwork   : function() {
                alert('Nincs stabil internetkapcsolat!');
            }
        });
});

function loadComponent() {
	$('.box, .box img').mouseenter(function() {
		$(this).prev('.light-layer').show();
		$(this).children('.light-layer').show();
	});
	
	$('.box').mouseleave(function() {
		$(this).find('.light-layer').hide();
	});
	
	$('#close').on('click', function() {
		$('#msg').slideUp(500).empty();
	});
}
