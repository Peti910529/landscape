<?php

namespace Snfapi\MainApplicationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    function getConfigTreeBuilder() {
        $builder = new TreeBuilder();
        $root = $builder->root ('myrtis_main_application');

        $this->addFacebookSection ($root);
        
        return $builder;
    }
    
    function addFacebookSection ($node) {
        $node
            ->children()
                ->arrayNode ('facebook')
                    ->children()
                        ->scalarNode ('app_id')
                            ->isRequired()
                        ->end()
                        ->scalarNode ('app_secret')
                            ->isRequired()
                        ->end()
                        ->scalarNode ('app_link')
                            ->isRequired()
                        ->end()
                        ->scalarNode ('page_id')
                            ->isRequired()
                        ->end()
                        ->scalarNode ('redirect_uri')
                            ->isRequired()
                        ->end()
                        ->scalarNode ('perms')
                        ->end()
						->scalarNode ('like_gate')
                        ->end()
						->scalarNode ('https')
                        ->end()
                        ->scalarNode ('mode')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
