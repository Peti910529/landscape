<?php
namespace Snfapi\MainApplicationBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ImageUpload {
	
	/**
     * @Assert\NotBlank(message = "Kérlek töltsd ki a Név mezőt!")
     */
    public $name;
	
	/**
	 * @Assert\NotBlank(message = "Kérlek válaszd ki a feltöltendő képet!")
     * @Assert\File(
     * 		maxSize = "10240k",
     *     	mimeTypes = {"image/jpeg", "image/jpg", "image/png"},
     *     	mimeTypesMessage = "Érvénytelen fájlformátum!",
	 *		maxSizeMessage = "A kép mérete túllépte a feltölthető maximális fájl méretét, mely korlát 10MB!",
	 *		notFoundMessage = "A fájl nem található!",
	 *		notReadableMessage = "A fájl nem olvasható!",
	 *		uploadIniSizeErrorMessage = "A kép mérete túllépte a feltölthető maximális fájl méretét, mely korlát 10MB!",
	 *		uploadFormSizeErrorMessage = "A kép mérete túllépte a feltölthető maximális fájl méretét, mely korlát 10MB!",
	 *		uploadErrorMessage = "Sikertelen fájlfeltöltés!"
     * )
     */
	public $image;
	
	public $path;
	
    /**
     * @param UploadedFile $file
     */
    function setFile(UploadedFile $image = null) {
        $this->image = $image;
    }

    function upload($file, $extension) {
		$this->path = $file.'.'.$extension;
		
		$image = $this->getFile()->move(
			$this->getUploadRootDir(),
			$this->path
		);
		
		$this->resize($image, $file, 265, 'small');
		$this->resize($image, $file, 600, 'medium');
		$this->resize($image, $file, 1800, 'mobile');
		
        $this->image = null;
    }

	/**
	 * Eredeti kép átméretezése a megadott méretbe
	 */
	function resize($original_image, $filename, $size, $folder) {
		//eredeti kép méreteinek a lekérése
		list($width, $height, $image_type) = getimagesize($original_image);
		
		switch(image_type_to_mime_type($image_type)) {
			case 'image/gif':
				$image = imagecreatefromgif($original_image);
				break;
			case 'image/pjpeg': case 'image/jpeg': case 'image/jpg':
				$image = imagecreatefromjpeg($original_image);
				break;
			case 'image/png': case 'image/x-png':
				$image = imagecreatefrompng($original_image);
				break;
		}
		
		//fekvő kép
		if($width > $height) {
			$scale =        $height/$width;
			$new_width =    $size;
			$new_height =   $size*$scale;
		//álló és kocka alakú kép
		} else {
			$scale =        $width/$height;
			$new_width =    $size*$scale;
			$new_height =   $size;
		}
		
		//átméretezés
		$image_final = imagecreatetruecolor($new_width, $new_height);
		imagecopyresampled($image_final, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		//kész kép mentése
		imagejpeg($image_final, 'uploads/images/'.$folder.'/'.$filename.'.jpg');
	}
	
	function getExtension() {
		return $this->getFile()->guessExtension();
	}

    /**
     * @return UploadedFile
     */
    function getFile() {
        return $this->image;
    }

    function getAbsolutePath() {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->path;
    }

    function getWebPath() {
        return null === $this->path
            ? null
            : $this->getUploadDir() . DIRECTORY_SEPARATOR . $this->path;
    }

    protected function getUploadRootDir() {
        return __DIR__ . '/../../../../web/'. $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/images/original';
    }
}
