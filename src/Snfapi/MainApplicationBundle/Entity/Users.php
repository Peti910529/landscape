<?php
namespace Snfapi\MainApplicationBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Users {
	/**
     * @Assert\NotBlank(message = "A Név mező nem lehet üres!")
     * @Assert\Length(min = "5")
	 * @Assert\Regex(
	 *		pattern="/^([a-záéíóöőúüűA-ZÁÉÍÓÖŐÚÜŰ.-\s]+) ([a-záéíóöőúüűA-ZÁÉÍÓÖŐÚÜŰ.-\s]+)$/",
	 *		message="A Név mezőnek tartalmaznia kell a vezeték és keresztnevet, illetve nem tartalmazhat számot vagy speciális karaktert!"
	 * )
     */
	public $name;
	
	/**
     * @Assert\NotBlank(message = "Az E-mail cím mező nem lehet üres!")
	 * @Assert\Email(message="Érvénytelen E-mail cím!")
     * @Assert\Length(min = "5")
     */
	public $email;
	
	public $news;
}
