<?php
namespace Snfapi\MainApplicationBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Comment {
	/**
     * @Assert\NotBlank(message = "Kérlek töltsd ki a Megjegyzés mezőt!")
     */
	public $content;
}
